FROM microsoft/dotnet:2.1.302-sdk AS build-env
ARG RESTORE
RUN : "${RESTORE:?Build argument needs to be set and non-empty.}"
WORKDIR /app

COPY ./src/ /app/
COPY ./Directory.Build.props /app


WORKDIR /app/Docitt.BusinessRules.Api
RUN $RESTORE && dotnet publish -c Release -o out --no-restore

# Build runtime image
FROM microsoft/dotnet:2.1.2-aspnetcore-runtime
WORKDIR /app/
COPY --from=build-env /app/Docitt.BusinessRules.Api/out .


ENTRYPOINT ["dotnet", "Docitt.BusinessRules.Api.dll"]