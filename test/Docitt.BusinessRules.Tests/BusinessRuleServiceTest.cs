﻿using Docitt.BusinessRules;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using RestSharp;
using Newtonsoft.Json;
using Microsoft.AspNet.Mvc;
using Docitt.Syndication.Plaid;

namespace Docitt.BusinessRules.Tests
{
    public class BusinessRuleServiceTest : BaseService
    {
        public BusinessRuleServiceTest()
        {
            LoggerFactory = new Mock<ILoggerFactory>();
            Logger = new Mock<ILogger>();

            Configuration = new Mock<IConfiguration>();

            DccClient = new Mock<IDccClient>();
            
            Configuration config = new Configuration();
            
            config.UnusualDepositRuleConfiguration = new UnusualDepositRuleConfiguration()
            {
                RuleValueInPercentage = 50,
                RuleValueInCurrency = 10000,
                PurchasePricePercentage = 2,
                MonthsToCheck = 2
            };

            Configuration.Setup(x => x.UnusualDepositRuleConfiguration).Returns(config.UnusualDepositRuleConfiguration);

            service = new BusinessRuleService(
                    LoggerFactory.Object,
                    Configuration.Object,
                    DccClient.Object
                );
            this.FillData();
        }

        private Mock<ILogger> Logger { get; }

        private Mock<ILoggerFactory> LoggerFactory { get; }
        private Mock<IConfiguration> Configuration { get; }
        private Mock<IDccClient> DccClient { get; }
       
        private BusinessRuleService service { get; }

        private BusinessRulesRequest BusinessRuleRequest;

        private List<UnUsualDepositTransactions> unusualtrns;
        private List<Transaction> FilteredTransactions;
        private List<string> Categories;

        private string ApplicantId { get; set; }

        private string AccountId { get; set; }

        [Fact]
        public async Task TestCheckBusinessRules()
        {
            this.Categories = this.Configuration.Object.TransactionCategoriesIgnoreList;
            System.Collections.Generic.List<string> accountIds = new System.Collections.Generic.List<string>();

            if (!string.IsNullOrWhiteSpace(this.AccountId))
            {
                accountIds.Add(this.AccountId);
            }

            DccClient.Setup(x => x.GetFilterTransactions(accountIds, this.BusinessRuleRequest.Income, Categories))
              .Returns(Task.Run(() => { return this.FilteredTransactions; }));
            
            var result = await service.LargeDepositsGivenAccountIds(accountIds, this.BusinessRuleRequest);
            Assert.Same(result, this.unusualtrns);
        }

        private void FillData()
        {
            this.AccountId = "Bza5Vqdw3GFLA8vrmw7PTPNlbJNVX3h7E1MKZ";

            this.ApplicantId = "ritesh.pk@sigmainfo.net";
 
            this.BusinessRuleRequest = JsonConvert.DeserializeObject<BusinessRulesRequest>(this.LoadJson("BusinessRuleRequest.json"));
            this.unusualtrns = JsonConvert.DeserializeObject<List<UnUsualDepositTransactions>>(this.LoadJson("BusinessRuleResponse.json"));
            this.FilteredTransactions = JsonConvert.DeserializeObject<List<Transaction>>(this.LoadJson("FilteredTransactions.json"));
            this.Categories = JsonConvert.DeserializeObject<List<string>>(this.LoadJson("Categories.json"));
        }
    }
}
