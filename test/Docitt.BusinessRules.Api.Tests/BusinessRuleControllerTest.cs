﻿using Docitt.BusinessRules.Api.Controllers;
using Microsoft.AspNet.Mvc;
using Microsoft.Framework.Caching.Memory;
using Moq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Docitt.BusinessRules.Api.Tests
{
    public class BusinessRuleControllerTest : BaseService
    {
        public BusinessRuleControllerTest()
        {
            FillData();
            this.BusinessRuleService = new Mock<IBusinessRuleService>();
        }

        private string ApplicantId { get; set; }

        private string AccountId { get; set; }

        private BusinessRulesRequest BusinessRuleRequest;

        private List<UnUsualDepositTransactions> result;

        private Mock<IBusinessRuleService> BusinessRuleService { get; set; }
        private Mock<IMemoryCache> Cache { get; set; }
        private Mock<IConfiguration> Configruation { get; set; }

        [Fact]
        public async Task CheckBusinessRule()
        {
            BusinessRuleService
                .Setup(x => 
                    x.LargeDepositsGivenApplicationId(this.ApplicantId, this.BusinessRuleRequest))
                .Returns(Task.FromResult<IList<UnUsualDepositTransactions>>(this.result));

            var controller = new BusinessRuleController(this.BusinessRuleService.Object);

            var result = await controller.CheckBusinessRule(this.AccountId, this.BusinessRuleRequest);

            var resultResponse = ((HttpOkObjectResult)result).Value as IList<UnUsualDepositTransactions>;

            Assert.Same(resultResponse, this.result);
        }

        private void FillData()
        {
            this.AccountId = "Bza5Vqdw3GFLA8vrmw7PTPNlbJNVX3h7E1MKZ";

            this.ApplicantId = "ritesh.pk@sigmainfo.net";

            this.BusinessRuleRequest = JsonConvert.DeserializeObject<BusinessRulesRequest>(this.LoadJson("BusinessRuleRequest.json"));

            this.result = JsonConvert.DeserializeObject<List<UnUsualDepositTransactions>>(this.LoadJson("BusinessRuleResponse.json"));
        }
    }
}
