﻿using System.IO;

namespace Docitt.BusinessRules.Api.Tests
{
    public class BaseService
    {
        /// <summary>
        /// LoadJson
        /// </summary>
        /// <param name="fileName">fileName</param>
        /// <returns>string</returns>
        public string LoadJson(string fileName)
        {
            string json = string.Empty;
            var currentPath = System.IO.Directory.GetCurrentDirectory();
            currentPath = currentPath.Replace(@"Docitt.BusinessRules.Api.Tests", string.Empty);
            var filePath = currentPath + @"DataSet\" + fileName;
            var filePath2 = currentPath + @"\test\DataSet\" + fileName;

            if (File.Exists(filePath))
            {
                var file = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                using (StreamReader r = new StreamReader(file))
                {
                    json = r.ReadToEnd();
                }

                return json;
            }

            if (File.Exists(filePath2))
            {
                var file = new FileStream(filePath2, FileMode.Open, FileAccess.Read);
                using (StreamReader r = new StreamReader(file))
                {
                    json = r.ReadToEnd();
                }

                return json;
            }

            return json;
        }
    }
}
