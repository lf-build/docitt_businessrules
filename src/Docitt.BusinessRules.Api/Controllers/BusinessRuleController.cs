﻿using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.BusinessRules.Api.Controllers
{
    /// <summary>
    /// Controller
    /// </summary>
    public class BusinessRuleController : ExtendedController
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="businessRuleService"></param>
        public BusinessRuleController(IBusinessRuleService businessRuleService)
        {
            this.BusinessRuleService = businessRuleService;
        }

        /// <summary>
        /// BusinessRuleService
        /// </summary>
        private IBusinessRuleService BusinessRuleService { get; }

        /// <summary>
        /// Check business rule
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="request"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("rules/account/{accountId}/largedeposits/{entityId?}")]
        public async Task<IActionResult> CheckBusinessRule(string accountId, [FromBody]BusinessRulesRequest request, [FromRoute] string entityId)
        {
            var accountIds = (new List<string>());
            accountIds.Add(accountId);

            return await ExecuteAsync(async () =>
            {
                return Ok(await BusinessRuleService.LargeDepositsGivenAccountIds(accountIds, request, entityId));
            });
        }

        /// <summary>
        /// check business rule by applicant
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="request"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("rules/applicant/{applicantId}/largedeposits/{entityId?}")]
        public async Task<IActionResult> CheckBusinessRuleByApplicant(string applicantId, [FromBody]BusinessRulesRequest request, [FromRoute] string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await BusinessRuleService.LargeDepositsGivenApplicationId(applicantId, request, entityId));
            });
        }
    }
}
