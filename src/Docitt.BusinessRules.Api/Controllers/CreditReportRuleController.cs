using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Web;
using Docitt.BusinessRules.Abstractions;

namespace Docitt.BusinessRules.Api.Controllers
{
    /// <summary>
    /// CreditReportController
    /// </summary>
    public class CreditReportRuleController : ExtendedController
    {
        /// <summary>
        /// CreditReportController
        /// </summary>
        /// <param name="creditReportRuleService">credit Report Service</param>
        /// <param name="logger">logger</param>
        public CreditReportRuleController(ICreditReportRuleService creditReportRuleService, ILogger logger)
        {
            this.CreditReportRuleService = creditReportRuleService;
            this.Log = logger;
        }

        #region Private Variables
        /// <summary>
        /// Get CreditReportService 
        /// </summary>
        private ICreditReportRuleService CreditReportRuleService { get;}

        /// <summary>
        /// Logger
        /// </summary>
        private ILogger Log { get; }

        #endregion

        /// <summary>
        /// GetCreditReportRule
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("creditReportRule/get/{applicationNumber}/{userName}")]
        [ProducesResponseType(typeof(CreditReportResponse), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public Task<IActionResult> GetCreditReportRule(string applicationNumber, string userName)
        {
            Log.Debug("Started GetCreditReportRule ...");
            return ExecuteAsync(async () =>
            {
                var result = await this.CreditReportRuleService.GetCreditReportRule(applicationNumber,userName);
                if(result == null)
                {
                     return NoContent();
                }
                return this.Ok(result);
            });
        }

        /// <summary>
        /// CreditReportDataGet
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("creditReportRule/post")]
        [ProducesResponseType(typeof(bool), 200)]
        public Task<IActionResult> CreditReportRulePost([FromBody]CreditReportRule requestPayload)
        {
            Log.Debug("Started CreditReportRulePost ...");
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.CreditReportRuleService.SaveCreditReportRule(requestPayload));
            });
        }


        /// <summary>
        /// ExecuteCreditReportRule
        /// </summary>
        /// <returns></returns>
         [HttpGet]
        [Route("creditReportRule/execute/{applicationNumber}/{userName}")]
        public Task<IActionResult> ExecuteCreditReportRule(string applicationNumber, string userName)
        {
            Log.Debug("Started ExecuteCreditReportRule ...");
            return ExecuteAsync(async () =>
            {
                await this.CreditReportRuleService.ExecuteCreditReportRule(applicationNumber,userName,string.Empty);
                return this.Ok();
            });
        }
    }
}