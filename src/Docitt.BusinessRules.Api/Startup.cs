﻿using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using Docitt.Syndication.Plaid.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Foundation.Services.Handlebars;
using System.Runtime;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;
using Docitt.BusinessRules.Abstractions;
using Docitt.BusinessRules.Persistence;
using LendFoundry.Clients.DecisionEngine;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using LendFoundry.Foundation.Persistence.Mongo;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace Docitt.BusinessRules.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            #if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "ActivityLog"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme() 
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> 
                { 
					{ "Bearer", new string[]{} }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Docitt.BusinessRules.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddTenantService();
            services.AddDccPlaidService();
            services.AddEventHub(Settings.ServiceName);
            services.AddDecisionEngine();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });

            services.AddTransient<IConfiguration>(
               provider => provider.GetRequiredService<IConfigurationService<Configuration>>().Get());

            // Service
            services.AddTransient<IDccClient, DccClient>();
            services.AddTransient<IBusinessRuleService, BusinessRuleService>();
            //services.AddTransient<ICreditRuleListener, CreditRuleListener>();

            services.AddTransient<ICreditReportRuleService, CreditReportRuleService>();
            services.AddTransient<ICreditRuleHelperService, CreditRuleHelperService>();
            //services.AddTransient<ICreditReportRuleServiceFactory, CreditReportRuleServiceFactory>();
            //services.AddTransient<ICreditRuleHelperServiceFactory, CreditRuleHelperServiceFactory>();
            services.AddTransient<ICreditReportRuleRepository, CreditReportRuleRepository>();

            // Register the Swagger generator, defining one or more Swagger documents
            // services.AddSwaggerDocumentation();

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {         
            app.UseHealthCheck();
		    app.UseCors(env);

            #if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Business rule");
            });
#else
            app.UseSwaggerDocumentation();
#endif

            app.UseConfigurationCacheDependency();
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
        }
    }
}