﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Docitt.BusinessRules
{
    public class FilterTransactionRequest : IFilterTransactionRequest
    {
        [JsonProperty("account_id")]
        public IList<string> AccountId { get; set; }

        [JsonProperty("applicant_id")]
        public string ApplicantId { get; set; }

        [JsonProperty("start_date")]
        public DateTime StartDate { get; set; }

        [JsonProperty("end_date")]
        public DateTime EndDate { get; set; }

        [JsonProperty("transaction_type")]
        public string TransactionType { get; set; }

        [JsonProperty("transaction_amount")]
        public double TransactionAmount { get; set; }

        [JsonProperty("categoryIds")]
        public IList<string> PlaidCategoryIds { get; set; }

        [JsonProperty("comparer")]
        public string Comparer { get; set; }
    }
}
