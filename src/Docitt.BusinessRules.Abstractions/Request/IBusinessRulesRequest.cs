﻿namespace Docitt.BusinessRules
{
    public interface IBusinessRulesRequest
    {
        double Income { get; set; }
        double? PurchasePrice { get; set; }
    }
}
