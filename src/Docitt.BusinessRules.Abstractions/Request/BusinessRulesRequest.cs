﻿using Newtonsoft.Json;

namespace Docitt.BusinessRules
{
    public class BusinessRulesRequest: IBusinessRulesRequest
    {
        [JsonProperty("income")]
        public double Income { get; set; }

        [JsonProperty("purchase_price")]
        public double? PurchasePrice { get; set; }
    }
}
