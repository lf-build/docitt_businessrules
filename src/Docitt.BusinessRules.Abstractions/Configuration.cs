﻿using System.Collections.Generic;

namespace Docitt.BusinessRules
{
    public class Configuration : IConfiguration
    {
        public UnusualDepositRuleConfiguration UnusualDepositRuleConfiguration { get; set; }

        public List<string> TransactionCategoriesIgnoreList { get; set; }

        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

        public EventMapping[] Events { get; set; }

        public string CreditReportRule { get; set; }

        public int CreditScore { get; set; }

        public int MortgageLateCount { get; set; }

        public int BankruptcyCount { get; set; }

        public int ForeClosureCount { get; set; }
    }
}
