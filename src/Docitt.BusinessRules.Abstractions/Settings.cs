﻿using System;

namespace Docitt.BusinessRules
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "docitt-business-rules";
    }
}
