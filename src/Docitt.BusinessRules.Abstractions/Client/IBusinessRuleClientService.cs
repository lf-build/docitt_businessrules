﻿using Docitt.Syndication.Plaid;
using Docitt.BusinessRules;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Docitt.BusinessRules
{
    public interface IBusinessRuleClientService
    {
        Task<IList<UnUsualDepositTransactions>> LargeDepositsGivenApplicantId(string applicantId, IBusinessRulesRequest businessRuleRequest, string entityId = null);

        Task<IList<Transaction>> LargeDepositsGivenAccountId(string accountId, IBusinessRulesRequest businessRuleRequest, string entityId = null);
    }
}
