﻿using LendFoundry.Security.Tokens;

namespace Docitt.BusinessRules
{
    public interface IBusinessRuleServiceClientFactory
    {
        IBusinessRuleClientService Create(ITokenReader reader);
    }
}
