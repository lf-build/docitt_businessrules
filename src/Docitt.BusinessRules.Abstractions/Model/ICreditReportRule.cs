using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;

namespace Docitt.BusinessRules.Abstractions
{
    public interface ICreditReportRule  : IAggregate
    {
         string ApplicationNumber { get; set; }

         string UserName { get; set; }
         string ReportId { get; set; }
         string RefreshId { get; set; }

          int EquifaxScore { get; set; }
         int ExperianScore { get; set; }
         int TransUnionScore { get; set; }

         int MortgageCount { get; set; }

         int BankRuptcyCount { get; set; }

         int ForeClosureCount { get; set; }
         bool RuleResult { get; set; }
         string Message { get; set; }
        string ReportType { get; set; }
        TimeBucket CreatedDateTime { get; set; }

         TimeBucket UpdatedDateTime { get; set; }
    }
}