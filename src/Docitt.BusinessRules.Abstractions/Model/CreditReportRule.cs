using Docitt.BusinessRules.Abstractions;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;

namespace Docitt.BusinessRules.Abstractions
{
    public class CreditReportRule : Aggregate,ICreditReportRule
    {
         public CreditReportRule()
        {
            CreatedDateTime = new TimeBucket(DateTimeOffset.Now);
        }
        public string ApplicationNumber { get; set; }
        public string UserName { get; set; }
        public string ReportId { get; set; }
        public string RefreshId { get; set; }

        public int EquifaxScore { get; set; }
        public int ExperianScore { get; set; }
        public int TransUnionScore { get; set; }

        public int MortgageCount { get; set; }

        public int BankRuptcyCount { get; set; }

        public int ForeClosureCount { get; set; }

        public bool RuleResult { get; set; }
        public string Message { get; set; }

        public string ReportType { get; set; }

         public TimeBucket CreatedDateTime { get; set; }

        public TimeBucket UpdatedDateTime { get; set; }

    }
}