﻿namespace Docitt.BusinessRules
{
    public class EventMapping
    {
        public string Name { get; set; }

         public string EventName { get; set; }

        public string ApplicationNumber { get; set; }

        public string ReportId { get; set; }

        public string UserName { get; set; }
    }
}
