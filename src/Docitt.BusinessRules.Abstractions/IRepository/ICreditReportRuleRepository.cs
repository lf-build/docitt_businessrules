using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;

namespace Docitt.BusinessRules.Abstractions
{
    public interface ICreditReportRuleRepository : IRepository<ICreditReportRule>
    {
         string AddCreditReportRule(ICreditReportRule data);

         Task<ICreditReportRule> GetRuleGivenApplicationAndUserName(string applicationNumber, string userName);
    }
}
