using LendFoundry.Security.Tokens;

namespace Docitt.BusinessRules.Abstractions
{
    public interface IRepositoryFactory
    {
         ICreditReportRuleRepository CreateCreditReportRuleRepository(ITokenReader reader);
    }
}