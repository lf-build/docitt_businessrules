﻿using Docitt.Syndication.Plaid;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.BusinessRules
{
    public interface IDccClient
    {
        Task<List<Transaction>> GetFilterTransactions(IList<string> accountId,  double income, List<string> categoryIds, string entityId = null);

        Task<IList<AccountsVM>> GetItemsWithAccounts(string applicantId, string entityId = null);
    }
}
