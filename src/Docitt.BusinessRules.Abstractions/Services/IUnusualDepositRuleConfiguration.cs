﻿namespace Docitt.BusinessRules
{
    public interface IUnusualDepositRuleConfiguration
    {
        int RuleValueInPercentage { get; set; }
        double RuleValueInCurrency { get; set; }
        int PurchasePricePercentage { get; set; }
        int MonthsToCheck { get; set; }
    }
}
