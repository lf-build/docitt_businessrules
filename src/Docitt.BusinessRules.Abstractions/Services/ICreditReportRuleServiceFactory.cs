using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace Docitt.BusinessRules.Abstractions
{
    public interface ICreditReportRuleServiceFactory
    {
         ICreditReportRuleService Create(ITokenReader reader, ITokenHandler handler, ILogger logger);
    }
}