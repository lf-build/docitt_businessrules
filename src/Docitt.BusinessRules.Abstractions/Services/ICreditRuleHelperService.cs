
using System.Threading.Tasks;

namespace Docitt.BusinessRules.Abstractions
{
    public interface ICreditRuleHelperService
    {
        //  Task<bool> GetCreditReportRuleResult(dynamic response, string applicationNumber, string userName);

         Task<CreditReportRule> GetCreditReportRuleResult(dynamic response, string applicationNumber, string userName);
    }
}