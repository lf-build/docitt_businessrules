using System;
using System.Threading.Tasks;
namespace Docitt.BusinessRules.Abstractions
{
    public interface ICreditReportRuleService
    {
         Task<ICreditReportResponse> GetCreditReportRule(string applicationNumber , string userName);

         Task<bool> SaveCreditReportRule(ICreditReportRule requestPayload);

         Task ExecuteCreditReportRule (string applicationNumber, string userName, string reportId);
    }
}