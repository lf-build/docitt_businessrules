﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace Docitt.BusinessRules
{
    public interface IConfiguration : IDependencyConfiguration
    {
        UnusualDepositRuleConfiguration UnusualDepositRuleConfiguration { get; set; }
        List<string> TransactionCategoriesIgnoreList { get; set; }

        EventMapping[] Events { get; set; }

          string CreditReportRule { get; set; }

         int CreditScore { get; set; }

         int MortgageLateCount { get; set; }

         int BankruptcyCount { get; set; }

         int ForeClosureCount { get; set; }
    }
}
