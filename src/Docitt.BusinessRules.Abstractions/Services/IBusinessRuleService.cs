﻿using Docitt.Syndication.Plaid;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.BusinessRules
{
    public interface IBusinessRuleService
    {
        Task<IList<UnUsualDepositTransactions>> LargeDepositsGivenApplicationId(string applicantId, BusinessRulesRequest request, string entityId = null);

        Task<List<Transaction>> LargeDepositsGivenAccountIds(IList<string> accountIds, BusinessRulesRequest request, string entityId = null);
    }
}
