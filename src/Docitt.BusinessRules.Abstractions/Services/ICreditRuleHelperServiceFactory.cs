using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace Docitt.BusinessRules.Abstractions
{
    public interface ICreditRuleHelperServiceFactory
    {
         ICreditRuleHelperService Create(ITokenReader reader, ITokenHandler handler, ILogger logger);

         
    }
}