﻿namespace Docitt.BusinessRules
{
    public class UnusualDepositRuleConfiguration : IUnusualDepositRuleConfiguration
    {
        public int RuleValueInPercentage { get; set; }
        public double RuleValueInCurrency { get; set; }
        public int PurchasePricePercentage { get; set; }
        public int MonthsToCheck { get; set; }
    }
}
