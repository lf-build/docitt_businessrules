﻿
using System.Collections.Generic;

namespace Docitt.BusinessRules
{
    public class UnUsualDepositTransactions : Docitt.Syndication.Plaid.Transaction
    {
        public string AccountMask { get; set; }
        public string InstitutionName { get; set; }
        public string InstitutionId { get; set; }

        public string AccountName { get; set; }

        public string AccountType { get; set; }

        public string AccountSubType { get; set; }

        public string CategoryDescription { get; set; }
    }
}
