﻿using System.Collections.Generic;

namespace Docitt.BusinessRules
{
    public interface IUnUsualDepositTransactions : Docitt.Syndication.Plaid.ITransaction
    {
        string AccountMask { get; set; }
        string InstitutionName { get; set; }
        string InstititionId { get; set; }
    }
}
