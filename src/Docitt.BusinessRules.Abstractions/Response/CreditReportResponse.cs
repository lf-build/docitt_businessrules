namespace Docitt.BusinessRules.Abstractions
{
    public class CreditReportResponse : ICreditReportResponse
    {
        public CreditReportResponse()
        {
            
        }
        
        public CreditReportResponse(ICreditReportRule creditReportRuleModel)
        {
            this.ApplicationNumber = creditReportRuleModel.ApplicationNumber;
            this.UserName = creditReportRuleModel.UserName;
            this.ReportId = creditReportRuleModel.ReportId;
            this.RefreshId = creditReportRuleModel.RefreshId;
            this.RuleResult = creditReportRuleModel.RuleResult;
            this.Message = creditReportRuleModel.Message;
            this.ReportType = creditReportRuleModel.ReportType;
        }

         public string ApplicationNumber { get; set; }
        public string UserName { get; set; }
        public string ReportId { get; set; }
        public string RefreshId { get; set; }

         public bool RuleResult { get; set; }
        public string Message { get; set; }

        public string ReportType { get; set; }
    }
}