namespace Docitt.BusinessRules.Abstractions
{
    public interface ICreditReportResponse
    {
          string ApplicationNumber { get; set; }
         string UserName { get; set; }
         string ReportId { get; set; }
         string RefreshId { get; set; }

          bool RuleResult { get; set; }
         string Message { get; set; }

         string ReportType { get; set; }
    }
}