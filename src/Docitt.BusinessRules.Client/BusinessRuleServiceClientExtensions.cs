﻿using System;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace Docitt.BusinessRules.Client
{
    public static class BusinessRuleServiceClientExtensions
    {       
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddBusinessRuleService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IBusinessRuleServiceClientFactory>(p => new BusinessRuleServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IBusinessRuleServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddBusinessRuleService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IBusinessRuleServiceClientFactory>(p => new BusinessRuleServiceClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IBusinessRuleServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddBusinessRuleService(this IServiceCollection services)
        {
            services.AddTransient<IBusinessRuleServiceClientFactory>(p => new BusinessRuleServiceClientFactory(p));
            services.AddTransient(p => p.GetService<IBusinessRuleServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
