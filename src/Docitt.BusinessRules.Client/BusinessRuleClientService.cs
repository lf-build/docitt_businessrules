﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.BusinessRules.Client
{
    public class BusinessRuleClientService : IBusinessRuleClientService
    {
        public BusinessRuleClientService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        /// <summary>
        /// LargeDepositsGivenApplicantId
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="businessRuleRequest">businessRuleRequest</param>
        /// <param name="entityId">entityId</param>
        /// <returns>Transaction list</returns>
        public async Task<IList<UnUsualDepositTransactions>> LargeDepositsGivenApplicantId(string applicantId, IBusinessRulesRequest businessRuleRequest, string entityId = null)
        {
            if (string.IsNullOrWhiteSpace(entityId))
            {
                return await Client.PostAsync<IBusinessRulesRequest, List<UnUsualDepositTransactions>>($"rules/applicant/{applicantId}/largedeposits", businessRuleRequest, true);
            }
            else
            {
                return await Client.PostAsync<IBusinessRulesRequest, List<UnUsualDepositTransactions>>($"rules/applicant/{applicantId}/largedeposits/{entityId}", businessRuleRequest, true);
            }
        }

        /// <summary>
        /// LargeDepositsGivenAccountId
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="businessRuleRequest">businessRuleRequest</param>
        /// <param name="entityId">entityId</param>
        /// <returns>transaction list</returns>
        public async Task<IList<Syndication.Plaid.Transaction>> LargeDepositsGivenAccountId(string accountId, IBusinessRulesRequest businessRuleRequest, string entityId = null)
        {
            if (string.IsNullOrWhiteSpace(entityId))
            {
                return await Client.PostAsync<IBusinessRulesRequest, List<Syndication.Plaid.Transaction>>($"rules/account/{accountId}/largedeposits", businessRuleRequest, true);
            }
            else
            {
                return await Client.PostAsync<IBusinessRulesRequest, List<Syndication.Plaid.Transaction>>($"rules/account/{accountId}/largedeposits/{entityId}", businessRuleRequest, true);
            }
        }
    }
}
