using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;
using System.Linq;
using System;
using Docitt.BusinessRules.Abstractions;
using System.Threading.Tasks;
using LendFoundry.Security.Encryption;

namespace Docitt.BusinessRules.Persistence
{
    /// <summary>
    /// CreditReportRuleRepository
    /// </summary>
    public class CreditReportRuleRepository : MongoRepository<ICreditReportRule, CreditReportRule>, ICreditReportRuleRepository
    {
        static CreditReportRuleRepository() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CreditReportRuleRepository"/> class.
        /// </summary>
        /// <param name="tenantService">tenantService</param>
        /// <param name="configuration">configuration</param>
        public CreditReportRuleRepository(
            ITenantService tenantService, 
            IMongoConfiguration configuration) : base(tenantService, configuration, "creditReportRuleResult")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(CreditReportRule)))
            {
                BsonClassMap.RegisterClassMap<CreditReportRule>(map =>
                {
                    map.AutoMap();
                    map.MapProperty(item => item.CreatedDateTime).SetIgnoreIfDefault(true);
                    map.MapProperty(item => item.UpdatedDateTime).SetIgnoreIfDefault(true);

                    var type = typeof(CreditReportRule);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }

            CreateIndexIfNotExists("tenant_id", Builders<ICreditReportRule>.IndexKeys
                .Ascending(item => item.TenantId), false);

            CreateIndexIfNotExists("application_number", Builders<ICreditReportRule>.IndexKeys
                .Ascending(item => item.TenantId)
                .Ascending(item => item.ApplicationNumber), false);

            CreateIndexIfNotExists("userName", Builders<ICreditReportRule>.IndexKeys
                .Ascending(item => item.TenantId)
                .Ascending(item => item.ApplicationNumber)
                .Ascending(item => item.UserName), false);
        }

        /// <summary>
        /// AddCreditReport
        /// </summary>
        /// <param name="IAssetReport">data</param>
        /// <returns>string</returns>
        public string AddCreditReportRule(ICreditReportRule data)
        {
            //// Update TenantId 
            data.TenantId = TenantService.Current.Id;

            //// Store AddCreditReport information in database 
            Collection
                .InsertOne(data);

            return "Add Credit Report Rule added successfully.";
        }

        /// <summary>
        /// GetRuleGivenApplicationAndUserName
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="userName">userName</param>
        /// <returns></returns>
        public Task<ICreditReportRule> GetRuleGivenApplicationAndUserName(string applicationNumber, string userName)
        {
            var result = Query
                    .Where(x => x.ApplicationNumber.Equals(applicationNumber)
                            && x.UserName.Equals(userName))
                    .FirstOrDefaultAsync();
                    
            return result;
        }
    }
}