﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Tenant.Client;
using Docitt.BusinessRules.Abstractions;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Encryption;

namespace Docitt.BusinessRules.Persistence
{
    public class RepositoryFactory : IRepositoryFactory
    {
        public RepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public ICreditReportRuleRepository CreateCreditReportRuleRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            
            var mongoConfigurationFactory =Provider.GetService<IMongoConfigurationFactory>();
            var mongoConfiguration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            return new CreditReportRuleRepository(tenantService, mongoConfiguration);
        }

        
    }
}
