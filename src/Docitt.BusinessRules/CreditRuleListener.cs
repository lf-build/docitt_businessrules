using System.Collections.Generic;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.EventHub;
using System;
using LendFoundry.Foundation.Listener;
using Docitt.BusinessRules.Abstractions;
using System.Linq;

namespace Docitt.BusinessRules
{
    public class CreditRuleListener : ListenerBase,ICreditRuleListener
    {
         public CreditRuleListener
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            ICreditReportRuleServiceFactory creditReportRuleServiceFactory
        ): base(tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)
        {

            this.EventHubFactory = eventHubFactory;
            this.ConfigurationFactory = configurationFactory;
            this.TokenHandler = tokenHandler;
            this.TenantServiceFactory = tenantServiceFactory;
            this.Logger = loggerFactory.Create(NullLogContext.Instance);
            this.CreditReportRuleServiceFactory = creditReportRuleServiceFactory;
            Logger.Debug($"Plaid Listner constructor");
        }


        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ILogger Logger { get; }

        private ICreditReportRuleServiceFactory CreditReportRuleServiceFactory { get; }

         public override List<string> GetEventNames(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            configurationService.ClearCache(Settings.ServiceName);
            var configuration = configurationService.Get();
            if (configuration == null)
            {
                return null;
            }
            else
            {
                return configuration.Events.Select(x=>x.Name).Distinct().ToList();
            }
        }
        
        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            try
            {                
                var token = TokenHandler.Issue(tenant, "CreditRuleIssuer");
                var reader = new StaticTokenReader(token.Value);
                var creditReportRuleService = CreditReportRuleServiceFactory.Create(reader, TokenHandler, Logger);

                var eventhub = EventHubFactory.Create(reader);

                var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
                configurationService.ClearCache(Settings.ServiceName);
                var configuration = configurationService.Get();
                if (configuration == null)
                {
                    logger.Error($"The configuration for service #{Settings.ServiceName} could not be found for {tenant} , please verify");
                    return null;
                }
                else
                {
                    logger.Info($"#{configuration.Events.Count()} entity(ies) found from configuration: {Settings.ServiceName}");

                    var uniqueEvents = configuration.Events.Distinct().ToList();

                    uniqueEvents.ForEach(eventConfig =>
                    {
                        eventhub.On(eventConfig.Name, UpdateStatus(eventConfig, this.Logger, creditReportRuleService)); //, itemService, assetReportService));
                        logger.Info($"It was made subscription to EventHub with the Event: #{eventConfig.Name} for tenant {tenant}");
                    });

                    return uniqueEvents.Select(x=>x.Name).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Unable to subscribe event for ${tenant}", ex);
                return new List<string>();
            }

        }

        private static Action<EventInfo> UpdateStatus(EventMapping eventConfiguration, ILogger logger, ICreditReportRuleService creditReportRuleService)
        {
            return @event =>
            {
                try
                {
                    logger.Info("eventName :");
                    var eventName = eventConfiguration.EventName.FormatWith(@event);
                    logger.Info($"{eventName}");

                    if (eventName == "creditReportNotification")
                    {
                        var applicationNumber = eventConfiguration.ApplicationNumber.FormatWith(@event);
                        var userName = eventConfiguration.UserName.FormatWith(@event);
                        var reportId = eventConfiguration.ReportId.FormatWith(@event);

                        logger.Info($"applicationNumber: {applicationNumber}; userName: { userName}; reportId: {reportId}");
                        //// TODO set the logic
                       
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }   
            };
        }
    }
}