using Docitt.BusinessRules.Abstractions;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Tenant.Client;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Docitt.BusinessRules
{
    public class CreditRuleHelperService  : ICreditRuleHelperService
    {
        #region Constructor

        public CreditRuleHelperService(ILogger logger,
            IConfiguration configuration,
            IDecisionEngineService decisionEngineService)
        {
            if (logger == null) throw new ArgumentException($"{nameof(logger)} is mandatory");
            if (configuration == null) throw new ArgumentException($"{nameof(configuration)} is mandatory");

            Logger = logger;
            DecisionEngineService = decisionEngineService;
            Configuration = configuration;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            client = new RestClient(); //TODO
        }

        #endregion Constructor

        #region Variable Declaration

        /// <summary>
        /// Gets Logger
        /// </summary>
        private ILogger Logger { get; }

        /// <summary>
        /// Gets DecisionEngineService
        /// </summary>
        private IDecisionEngineService DecisionEngineService { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IRestClient client { get; }

        #endregion Variable Declaration

        #region Methods
        // <summary>
        /// CreateItemExecutionFlow
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<bool> GetCreditReportResonseFromNAF(string applicationNumber, string userName)
        {
            return await Task.Run(() => { return true; });
        }


        public async Task<CreditReportRule> GetCreditReportRuleResult(dynamic response, string applicationNumber, string userName)
        {
            return await Task.Run(() =>
            {
                try
                {
                    Logger.Info($"GetCreditReportRuleResult start...");
                    Logger.Info($"...Execute DecisionEngine");
                    // var masterResult = await AdapterService.GetEnumFieldDetail();
                    // var tenantId = TenantService.Current.Id;
                    var payload = "Temp";
                    // payload.BorrowerId = borrowerId;
                    var computeResult = DecisionEngineService.Execute<dynamic, CreditReportRule>("creditReport", new { payload = payload });
                    // Logger.Info($"...Executed");
                    // if (computeResult.Result == "Passed")
                    //     return computeResult;
                    // else
                    //     return null;

                    return computeResult;
                }
                catch (Exception ex)
                {
                    Logger.Error($"[ERROR] in the method GetCreditReportRuleResult : {ex}");
                    throw new ArgumentException($"{ex}");
                }
            });
        }

        #endregion Methods
    }
}