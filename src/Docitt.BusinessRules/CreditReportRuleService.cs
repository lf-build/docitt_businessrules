using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Docitt.BusinessRules.Abstractions;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;

namespace Docitt.BusinessRules {
    public class CreditReportRuleService : ICreditReportRuleService 
    {
        public CreditReportRuleService (ILogger logger, IConfiguration configuration, ICreditReportRuleRepository creditReportRuleRepository,
                                             ICreditRuleHelperService  creditRuleHelperService) 
        {
            if (configuration == null) throw new ArgumentException ("Configuration cannot be found, please check");
            if (creditReportRuleRepository == null) throw new ArgumentException ("creditReportRuleRepository cannot be found, please check");
            if (creditRuleHelperService == null) throw new ArgumentException ("creditRuleHelperService cannot be found, please check");

            this.Logger = logger;
            this.Configuration = configuration;
            this.CreditReportRuleRepository = creditReportRuleRepository;
            this.CreditRuleHelperService = creditRuleHelperService;
        }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets creditReportRuleRepository
        /// </summary>
        private ICreditReportRuleRepository CreditReportRuleRepository { get; }

        /// <summary>
        /// Gets CreditRuleHelperService
        /// </summary>
        private ICreditRuleHelperService CreditRuleHelperService {get;}

        private ILogger Logger { get; }

        /// <summary>
        /// Gets GetCreditReportRule
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="userName">userName</param>
        /// <returns>Response</returns>
        public async Task<ICreditReportResponse> GetCreditReportRule (string applicationNumber, string userName) {
            Logger.Debug ("GetCreditReportRule Started ...");
            ICreditReportResponse response = null;

            if (string.IsNullOrEmpty (applicationNumber))
                throw new Exception ("Missing applicationNumber");

            if (string.IsNullOrEmpty (userName))
                throw new Exception ("Missing userName");

            var result = await CreditReportRuleRepository.GetRuleGivenApplicationAndUserName (applicationNumber, userName);

            if(result == null)
            {
                return response;
            }

            response = new CreditReportResponse(result);
            return response;
        }

        /// <summary>
        /// Gets SaveCreditReportRule
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="userName">userName</param>
        /// <returns>Response</returns>
        public async Task<bool> SaveCreditReportRule (ICreditReportRule requestPayload)
        {
            return await Task.Run(() =>
            {
                Logger.Debug ("SaveCreditReportRule Started ...");

                if (requestPayload == null)
                    throw new Exception ("Data are null");

                var result = this.CreditReportRuleRepository.AddCreditReportRule (requestPayload);

                return true;
            });
        }

        /// <summary>
        /// ExecuteCreditReportRule
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="userName">userName</param>
        /// <param name="reportId">reportId</param>
        public async Task ExecuteCreditReportRule (string applicationNumber, string userName, string reportId) {
            Logger.Debug ("ExecuteCreditReportRule Started ...");

            if (string.IsNullOrEmpty (applicationNumber))
                throw new Exception ("Missing applicationNumber");

            if (string.IsNullOrEmpty (userName))
                throw new Exception ("Missing userName");

            var result = await CreditReportRuleRepository.GetRuleGivenApplicationAndUserName (applicationNumber, userName);
            var isExist = result != null ? true : false;

            ////Step : 1 :  TODO: call endpoint for NAF Report

            ////Step : 2 : Pass this reference to Rule:
            var responseFromRule = await this.CreditRuleHelperService.GetCreditReportRuleResult(null, applicationNumber, userName);


            /// Step : 3 : insert or update data in the database on Rule Response
            
            responseFromRule.ApplicationNumber = applicationNumber; //TODO remove
            responseFromRule.UserName = userName;//TODO : remove
            await InsertOrUpdateCreditReportRule(responseFromRule , isExist);
        }

        #region Private methods

        private async Task InsertOrUpdateCreditReportRule (ICreditReportRule result, bool isExist) 
        {
            result.UpdatedDateTime = new TimeBucket (DateTimeOffset.Now);
             await Task.Run(() => {
                if (isExist) 
                {
                    this.CreditReportRuleRepository.Update (result);
                } 
                else
                {
                    this.CreditReportRuleRepository.Add (result);
                }
             });
        }

        #endregion
    }
}