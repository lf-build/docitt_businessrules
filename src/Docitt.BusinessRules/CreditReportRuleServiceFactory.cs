using System;
using Docitt.BusinessRules.Abstractions;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;

namespace Docitt.BusinessRules
{
    public class CreditReportRuleServiceFactory : ICreditReportRuleServiceFactory
    {
        public CreditReportRuleServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public ICreditReportRuleService Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configurationService = configurationServiceFactory.Create<Configuration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();

            var repositoryFactory = Provider.GetService<IRepositoryFactory>();
            var creditReportRuleRepository = repositoryFactory.CreateCreditReportRuleRepository(reader);

            var creditRuleHelperFactory = Provider.GetService<ICreditRuleHelperServiceFactory>();
            var creditReportRuleHelperSevice = creditRuleHelperFactory.Create(reader,handler,logger);

            return new CreditReportRuleService(logger, configuration,creditReportRuleRepository,creditReportRuleHelperSevice);
        }
    }
}