using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Configuration;
using System;
using Docitt.BusinessRules.Abstractions;
using LendFoundry.Clients.DecisionEngine;
using Microsoft.Extensions.DependencyInjection;

namespace Docitt.BusinessRules
{
    public class CreditRuleHelperServiceFactory : ICreditRuleHelperServiceFactory
    {
        public CreditRuleHelperServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public ICreditRuleHelperService Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configurationService = configurationServiceFactory.Create<Configuration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();

            var decisionServiceFactory = Provider.GetService<IDecisionEngineClientFactory>();
            var decisionService = decisionServiceFactory.Create(reader);
            return new CreditRuleHelperService(logger, configuration,decisionService);
        }
    }
}