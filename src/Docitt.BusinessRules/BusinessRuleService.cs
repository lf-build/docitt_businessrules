﻿using Docitt.Syndication.Plaid;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Docitt.BusinessRules
{
    public class BusinessRuleService : IBusinessRuleService
    {
        public BusinessRuleService
        (
            ILoggerFactory loggerFactory,
            IConfiguration configuration,
            IDccClient dccClient)
        {
            Configuration = configuration;
            DccClient = dccClient;
            Logger = loggerFactory.Create(NullLogContext.Instance);
            Logger.Info($"BusinessRuleService Constructor");
        }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }
        private IDccClient DccClient { get; }
        private CommandExecutor CommandExecutor { get; }
        private ILogger Logger { get; }

        /// <summary>
        /// LargeDepositsGivenApplicationId
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="request"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<IList<UnUsualDepositTransactions>> LargeDepositsGivenApplicationId(string applicantId, BusinessRulesRequest request, string entityId = null)
        {
            Logger.Debug($"LargeDepositsGivenApplicationId started for {entityId} and applicant {applicantId} ");
            
            IList<UnUsualDepositTransactions> result = new List<UnUsualDepositTransactions>();

            //// Get list of items along with their accounts
            var items = await DccClient.GetItemsWithAccounts(applicantId, entityId) as List<AccountsVM>;
            if (items != null && items.Count > 0)
            {
                //// Parse result and store in dictionary object 
                var accountsDictionary = ParseInstitutionList(items);

                //// Get the transactions for the string[] accounts
                var transactions = await LargeDepositsGivenAccountIds(accountsDictionary.Keys.ToList(), request, entityId);

                //// Generate UnUsualDepositTransactions response object
                transactions.ForEach(t =>
                {
                    var trnsString = JsonConvert.SerializeObject(t);
                    var ut = JsonConvert.DeserializeObject<UnUsualDepositTransactions>(trnsString);
                    ut.InstitutionName = accountsDictionary[ut.PlaidAccountId].InstitutionName;
                    ut.InstitutionId = accountsDictionary[ut.PlaidAccountId].InstitutionId;
                    ut.AccountMask = accountsDictionary[ut.PlaidAccountId].AccountMask;
                    ut.AccountName = accountsDictionary[ut.PlaidAccountId].AccountName;
                    ut.AccountType = accountsDictionary[ut.PlaidAccountId].AccountType;
                    ut.AccountSubType = accountsDictionary[ut.PlaidAccountId].AccountSubType;

                    // Temporary set last index of category array for description
                    ut.CategoryDescription = ut.Category != null ? ut.Category.Last() : null;

                    result.Add(ut);
                });
            }
            Logger.Debug($"LargeDepositsGivenApplicationId ended for {entityId} and applicant {applicantId}");
            return result;
        }

        /// <summary>
        /// LargeDepositsGivenAccountIds
        /// </summary>
        /// <param name="accountIds"></param>
        /// <param name="request"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<List<Transaction>> LargeDepositsGivenAccountIds(IList<string> accountIds, BusinessRulesRequest request, string entityId = null)
        {
            Logger.Debug($"LargeDepositsGivenAccountIds started for {entityId} ");

            var thresholdAmount = CalculateThresholdAmount(
                request.Income,
                Configuration.UnusualDepositRuleConfiguration.RuleValueInPercentage,
                Configuration.UnusualDepositRuleConfiguration.RuleValueInCurrency,
                request.PurchasePrice ?? 0.0,
                Configuration.UnusualDepositRuleConfiguration.PurchasePricePercentage
                );

            var transList = await DccClient.GetFilterTransactions(
                                    accountIds,
                                    thresholdAmount,
                                    Configuration.TransactionCategoriesIgnoreList,
                                    entityId);

            Logger.Debug($"transList.count : {transList.Count.ToString()} for {entityId} ");

            return transList;
        }

        /// <summary>
        /// Calculate Threshold
        /// </summary>
        /// <param name="i">income</param>
        /// <param name="i_per">income percent</param>
        /// <param name="limit">min limit</param>
        /// <param name="p">purchase price</param>
        /// <param name="p_per">purchase price percent</param>
        /// <returns></returns>
        private double CalculateThresholdAmount(double i, double i_per, double limit, double p, double p_per)
        {
            var thresholdAmount = Math.Min((i * i_per) / 100, limit);

            if (p > 0.0)
            {
                thresholdAmount = Math.Min(thresholdAmount, (p * p_per) / 100);
            }
            Logger.Debug($"CalculateThresholdAmount : {thresholdAmount}");
            return thresholdAmount;
        }

        private IDictionary<string, UnUsualDepositTransactions> ParseInstitutionList(List<AccountsVM> items)
        {
            IDictionary<string, UnUsualDepositTransactions> dicAccount = new Dictionary<string, UnUsualDepositTransactions>();

            //// Parse AccountsVM object and generate string[] accounts & dictionary accounts, institution
            items.ForEach(it =>
                it.Accounts.ForEach(ac =>
                {
                    // Dictionary Object 
                    dicAccount.Add(ac.PlaidAccountId, new UnUsualDepositTransactions
                    {
                        InstitutionId = it.InstitutionId,
                        InstitutionName = it.Name,
                        AccountMask = ac.Mask,
                        AccountName = ac.AccountName,
                        AccountType = ac.AccountType,
                        AccountSubType = ac.SubType,
                    });
                })
            );

            return dicAccount;
        }
    }
}
