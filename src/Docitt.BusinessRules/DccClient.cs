﻿using Docitt.Syndication.Plaid;
using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Docitt.BusinessRules
{
    public class DccClient : IDccClient
    {
        public DccClient(
            ILoggerFactory loggerFactory,
            IConfiguration configuration,
            IDccPlaidServiceFactory dccPlaidServiceFactory,
            ITokenHandler tokenParser,
            ITokenReader tokenReader)
        {
            if (tokenReader == null) throw new ArgumentException($"{nameof(tokenReader)} is mandatory");
            if (tokenParser == null) throw new ArgumentException($"{nameof(tokenParser)} is mandatory");
            if (configuration == null) throw new ArgumentException("Configuration cannot be found, please check");

            Logger = loggerFactory.Create(NullLogContext.Instance);
            Logger.Info($"DccClient Constructor");

            //// Configuration
            Configuration = configuration;

            //// Create Client object 
            Client = dccPlaidServiceFactory.Create(
                new StaticTokenReader(tokenParser.Parse(tokenReader.Read()).Value));

            Logger.Info($"DccClient Constructor Completed...");
        }

        /// <summary>
        /// Plaid Service Client 
        /// </summary>
        private IDccPlaidService Client { get; }

        /// <summary>
        /// Logger Object
        /// </summary>
        private ILogger Logger { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Filter Transactions 
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <param name="applicantId">Applicant Id</param>
        /// <param name="threshold">Threshold Amount</param>
        /// <param name="categoryIds">List of exception categories</param>
        /// <returns>list of transactions</returns>
        public async Task<List<Transaction>> GetFilterTransactions(IList<string> accountId, double threshold, List<string> categoryIds, string entityId = null)
        {
            Logger.Info($"GetFilterTransactions...");
            DateTime startDate = DateTime.Now.AddDays(-(Configuration.UnusualDepositRuleConfiguration.MonthsToCheck * 30));
            DateTime endDate = DateTime.Now;

            return await Client.GetFilterTransactions(startDate, endDate, accountId, null, threshold, categoryIds, entityId);
        }

        /// <summary>
        /// Get the Item Details including accounts
        /// </summary>
        /// <param name="applicantId">applicant id</param>
        /// <returns>item details</returns>
        public async Task<IList<AccountsVM>> GetItemsWithAccounts(string applicantId, string entityId = null)
        {
            Logger.Info($"GetItemsWithAccounts... ApplicantId : {applicantId} amd EntityId: {entityId}");
            return await Client.GetItemsWithAccounts(applicantId, entityId);
        }
    }
}
